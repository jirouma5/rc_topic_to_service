cmake_minimum_required(VERSION 3.1.2)
project(service_parser)

set(CATKIN_DEPENDENCIES
  roscpp
  cmake_modules
  mrs_msgs
  roslaunch
  #nav_msgs
  std_srvs
  #nodelet
  #mrs_uav_managers
  #mrs_lib
  #dynamic_reconfigure
  #message_generation
  #geometry_msgs
  )

find_package(catkin REQUIRED COMPONENTS
  ${CATKIN_DEPENDENCIES}
  )

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

#generate_messages(DEPENDENCIES
#  std_msgs
  #geometry_msgs
#  )

set(EXECUTABLES
  ServiceParser
  )

catkin_package(
  CATKIN_DEPENDS ${CATKIN_DEPENDENCIES}
  LIBRARIES ${LIBRARIES}
  #DEPENDS Eigen
  )

include_directories(
  ${catkin_INCLUDE_DIRS}
  )

# parser

add_executable(ServiceParser
  src/main.cpp
  )

#add_library(ServiceParser
#  src/main.cpp
#  )

add_dependencies(ServiceParser
  ${${PROJECT_NAME}_EXPORTED_TARGETS}
  ${catkin_EXPORTED_TARGETS}
  )

target_link_libraries(ServiceParser
  ${catkin_LIBRARIES}
  )

## --------------------------------------------------------------
## |                           Install                          |
## --------------------------------------------------------------

install(TARGETS ${EXECUTABLES}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION}
  )

#install(TARGETS ServiceParser
#  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
#)

install(DIRECTORY config
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
  )

install(FILES plugins.xml
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
  )
