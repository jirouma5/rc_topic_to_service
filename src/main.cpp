#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include "mrs_msgs/Vec4.h"
#include <cstdlib>
#include "std_srvs/SetBool.h"
#include "std_srvs/Trigger.h"

// arm/disarm
#define ARM_TOPIC "arm"
#define DISARM_TOPIC "disarm"
#define ARM_SERVICE "hw_api/arming"

// takeoff/land
#define TAKEOFF_TOPIC "takeoff"
#define TAKEOFF_SERVICE "uav_manager/takeoff"
#define OFFBOARD_SERVICE "hw_api/offboard"

#define LAND_TOPIC "land"
#define LAND_SERVICE "uav_manager/land"

// intercept
#define START_INTERCEPT_TOPIC "interceptor/start"
#define STOP_INTERCEPT_TOPIC "interceptor/stop"
#define INTERCEPTOR_INTERCEPT_SERVICE "interceptor/intercept"

#define ENABLE_INTERCEPT_TOPIC "interceptor/enable"
#define DISABLE_INTERCEPT_TOPIC "interceptor/disable"
#define INTERCEPTOR_ENABLE_SERVICE "interceptor/enable"

class ServiceParser {
public:
    ServiceParser() {
        // Initialize the ROS node
        ros::NodeHandle nh;

        // arm/disarm
        subArm = nh.subscribe(ARM_TOPIC, 10, &ServiceParser::armCallback, this);
        subDisarm = nh.subscribe(DISARM_TOPIC, 10, &ServiceParser::disarmCallback, this);
        clientArm = nh.serviceClient<std_srvs::SetBool>(ARM_SERVICE);

        // takeoff/land
        subTakeoff = nh.subscribe(TAKEOFF_TOPIC, 10, &ServiceParser::takeoffCallback, this);
        clientTakeoff = nh.serviceClient<std_srvs::Trigger>(TAKEOFF_SERVICE);
        clientOffboard = nh.serviceClient<std_srvs::Trigger>(OFFBOARD_SERVICE);

        subLand = nh.subscribe(LAND_TOPIC, 10, &ServiceParser::landCallback, this);
        clientLand = nh.serviceClient<std_srvs::Trigger>(LAND_SERVICE);

        // intercept
        subStartIntercept = nh.subscribe(START_INTERCEPT_TOPIC, 10, &ServiceParser::startInterceptCallback, this);
        subStopIntercept = nh.subscribe(STOP_INTERCEPT_TOPIC, 10, &ServiceParser::stopInterceptCallback, this);
        clientInterceptorIntercept = nh.serviceClient<std_srvs::SetBool>(INTERCEPTOR_INTERCEPT_SERVICE);

        subEnableIntercept = nh.subscribe(ENABLE_INTERCEPT_TOPIC, 10, &ServiceParser::enableInterceptCallback, this);
        subDisableIntercept = nh.subscribe(DISABLE_INTERCEPT_TOPIC, 10, &ServiceParser::disableInterceptCallback, this);
        clientInterceptorEnable = nh.serviceClient<std_srvs::SetBool>(INTERCEPTOR_ENABLE_SERVICE);
    }

    void armCallback(const std_msgs::Bool::ConstPtr& msg) {
        if (msg->data) {
            callArmService(true);
        }
    }

    void disarmCallback(const std_msgs::Bool::ConstPtr& msg) {
        if (msg->data) {
            callArmService(false);
        }
    }

    void takeoffCallback(const std_msgs::Bool::ConstPtr& msg) {
        if (msg->data) {
            callTakeoffService();
        }
    }

    void landCallback(const std_msgs::Bool::ConstPtr& msg) {
        if (msg->data) {
            callLandService();
        }
    }

    void startInterceptCallback(const std_msgs::Bool::ConstPtr& msg) {
        if (msg->data) {
            callInterceptorInterceptService(true);
        }
    }

    void stopInterceptCallback(const std_msgs::Bool::ConstPtr& msg) {
        if (msg->data) {
            callInterceptorInterceptService(false);
        }
    }

    void enableInterceptCallback(const std_msgs::Bool::ConstPtr& msg) {
        if (msg->data) {
            callInterceptorEnableService(true);
        }
    }

    void disableInterceptCallback(const std_msgs::Bool::ConstPtr& msg) {
        if (msg->data) {
            callInterceptorEnableService(false);
        }
    }

    void callArmService(bool value) {
        std_srvs::SetBool srv;

		srv.request.data = value;

        if (clientArm.call(srv)) {
            ROS_INFO("Service call successful");
        } else {
            ROS_ERROR("Failed to call service");
        }
    }

    void callTakeoffService() {
        std_srvs::Trigger srv;

        // call offboard
        if (clientOffboard.call(srv)) {
            ROS_INFO("Service call successful");
        } else {
            ROS_ERROR("Failed to call service");
        }

        // Call the service
        if (clientTakeoff.call(srv)) {
            ROS_INFO("Service call successful");
        } else {
            ROS_ERROR("Failed to call service");
        }
    }

    void callLandService() {
        std_srvs::Trigger srv;

        if (clientLand.call(srv)) {
            ROS_INFO("Service call successful");
        } else {
            ROS_ERROR("Failed to call service");
        }
    }

    void callInterceptorInterceptService(bool value) {
        std_srvs::SetBool srv;

		srv.request.data = value;

        if (clientInterceptorIntercept.call(srv)) {
            ROS_INFO("Service call successful");
        } else {
            ROS_ERROR("Failed to call service");
        }
    }

    void callInterceptorEnableService(bool value) {
        std_srvs::SetBool srv;

		srv.request.data = value;

        if (clientInterceptorEnable.call(srv)) {
            ROS_INFO("Service call successful");
        } else {
            ROS_ERROR("Failed to call service");
        }
    }

private:

    // arm/disarm
    ros::Subscriber subArm;
    ros::Subscriber subDisarm;
    ros::ServiceClient clientArm;

    // takeoff/land
    ros::Subscriber subTakeoff;
    ros::ServiceClient clientTakeoff;
    ros::ServiceClient clientOffboard;

    ros::Subscriber subLand;
    ros::ServiceClient clientLand;

    // intercept
    //      enable
    ros::Subscriber subEnableIntercept;
    ros::Subscriber subDisableIntercept;
    ros::ServiceClient clientInterceptorEnable;

    //      start
    ros::Subscriber subStartIntercept;
    ros::Subscriber subStopIntercept;
    ros::ServiceClient clientInterceptorIntercept;
};

int main(int argc, char** argv) {
    // Initialize the ROS node
    ros::init(argc, argv, "service_parser");

    // Create an instance of the MyNode class
    ServiceParser myNode;

    // Spin to keep the node alive
    ros::spin();

    return 0;
}
